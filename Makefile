SHELL=/bin/bash
#IP=172.24.1.1
IP=10.42.0.240
user=root

all: build deploy

init: build prepareremote uploaddirectly uploadservice enableservice startservice

deploy: uploadstatic uploadtoswp swapswpandreload

build:
	@# For static build
	@# CGO_ENABLED=0 GOOS=linux GOARCH=arm go build -o ./deploy/emil -a -ldflags '-extldflags "-static"' .
	GOOS=linux GOARCH=arm go build -o ./deploy/emil .

prepareremote:
	ssh $(user)@$(IP) 'mkdir -p ~/emil;mkdir -p /etc/systemd/system/'

uploaddirectly:
	scp ./deploy/emil $(user)@$(IP):~/emil

uploadstatic:
	scp -r ./static/* $(user)@$(IP):~/emil/static

uploadtoswp:
	scp ./deploy/emil $(user)@$(IP):~/emil/emil_swp

swapswpandreload:
	@# This is in single command to make it quick
	ssh $(user)@$(IP) 'systemctl stop robotemil;mv ~/emil/emil_swp ~/emil/emil;systemctl start robotemil'

uploadservice:
	scp ./deploy/robotemil.service $(user)@$(IP):/etc/systemd/system/
	systemctl daemon-reload

restartservice:
	ssh $(user)@$(IP) systemctl restart robotemil

statusservice:
	ssh $(user)@$(IP) systemctl status robotemil

stopservice:
	ssh $(user)@$(IP) systemctl stop robotemil

startservice:
	ssh $(user)@$(IP) systemctl start robotemil

enableservice:
	ssh $(user)@$(IP) systemctl enable robotemil

shell:
	ssh $(user)@$(IP)