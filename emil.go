package main

import (
	"fmt"
	"log"
	"net/http"
	"path/filepath"
	"strconv"
	"strings"

	l "gitlab.com/sijisu/rplidar"
	"go.mongodb.org/mongo-driver/bson"
)

type Emil struct {
	motors         *Motors
	lidar          *l.RPLidar
	stopChan       chan bool
	latestScanChan chan []l.Measurement
}

type Measurement struct {
	IsNewScan bool    `bson:"n"`
	Quality   int     `bson:"q"`
	Angle     float64 `bson:"a"`
	Distance  float64 `bson:"d"`
}

type Scan struct {
	Scan []Measurement
}

func (e *Emil) roothandler(w http.ResponseWriter, r *http.Request) {
	status, _ := e.lidar.GetHealth()
	fmt.Fprintf(w, "Status: %s", status)
}

func (e *Emil) runhandler(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path[len("/run/"):]
	speeds := strings.Split(path, "/")
	if len(speeds) != 2 {
		fmt.Fprintf(w, "Wrong params")
		return
	}
	speedL, err := strconv.Atoi(speeds[0])
	if err != nil {
		fmt.Fprintf(w, "Wrong param L: %s", err)
		return
	}
	speedR, err := strconv.Atoi(speeds[1])
	if err != nil {
		fmt.Fprintf(w, "Wrong param R: %s", err)
		return
	}
	e.motors.runContinuous(speedL, speedR)
	fmt.Fprintf(w, "%d %d", speedL, speedR)
}

func (e *Emil) getscanhandler(w http.ResponseWriter, r *http.Request) {
	latestScan := <-e.latestScanChan
	w.Header().Set("Content-Type", "application/bson")
	//p := Scan{*(*[]Measurement)(unsafe.Pointer(&latestScan))}
	res, err := bson.Marshal(latestScan)
	if err != nil {
		panic(err)
	}
	//fmt.Fprint(w, res)
	w.Write(res)
}

func (e *Emil) starthandler(w http.ResponseWriter, r *http.Request) {
	isrunning, _ := e.lidar.IsRunning()
	if isrunning {
		fmt.Fprintf(w, "Already running")
		return
	}
	e.stopChan = make(chan bool)
	measChan, stopLidarChan := e.lidar.IterScans(0, 0)
	go func() {
		defer func() {
			e.lidar.StopMotor()
			close(stopLidarChan)
		}()

		for s := range measChan {
			select {
			case e.latestScanChan <- s:
				// Nothing
			default:
				<-e.latestScanChan
				e.latestScanChan <- s
			}

			// Close
			select {
			default:
				// Nothing
			case <-e.stopChan:
				return
			}
		}
	}()
	fmt.Fprintf(w, "OK")
}

func (e *Emil) stophandler(w http.ResponseWriter, r *http.Request) {
	isrunning, _ := e.lidar.IsRunning()
	if !isrunning {
		fmt.Fprintf(w, "Not running")
		return
	}
	close(e.stopChan)
	select {
	case <-e.latestScanChan:
		// Nothing
	default:
		// Nothing
	}
	fmt.Fprintf(w, "OK")
}

func (e *Emil) pinghandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("pong")
	fmt.Fprintf(w, "pong")
}

func main() {
	fmt.Println("Starting RobotEmil Alpha...")

	matches, err := filepath.Glob("/dev/ttyUSB*")
	if err != nil {
		panic(err)
	}
	if len(matches) == 0 {
		panic("RPiLidar not found")
	}

	e := Emil{}

	e.lidar, _ = l.NewRPLidar(matches[0], 0, false)
	e.lidar.StopMotor()
	e.motors, err = NewMotors()
	if err != nil {
		panic(err)
	}

	e.latestScanChan = make(chan []l.Measurement, 1)

	http.HandleFunc("/", e.roothandler)
	http.HandleFunc("/run/", e.runhandler)
	http.HandleFunc("/start", e.starthandler)
	http.HandleFunc("/stop", e.stophandler)
	http.HandleFunc("/scan", e.getscanhandler)
	http.HandleFunc("/ping", e.pinghandler)
	http.Handle("/static/", http.StripPrefix("/static", http.FileServer(http.Dir("./static/"))))
	log.Fatal(http.ListenAndServe(":8080", nil))
}
