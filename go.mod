module gitlab.com/sijisu/robotemil

go 1.16

require (
	github.com/Sijisu/gpio v0.0.0-20210831112433-819ea3bf3680
	github.com/davecheney/gpio v0.0.0-20160912024957-a6de66e7e470 // indirect
	gitlab.com/sijisu/rplidar v1.0.4
	go.mongodb.org/mongo-driver v1.7.2
	golang.org/x/sys v0.0.0-20210908233432-aa78b53d3365 // indirect
)
