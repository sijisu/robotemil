package main

import (
	"github.com/Sijisu/gpio"
)

type Motor struct {
	forward       *PWMPin
	backward      *PWMPin
	encoder       gpio.Pin
	absoluteSteps uint
}

func NewMotor(forwardPin uint8, backwardPin uint8, encoderPin uint8) (*Motor, error) {
	forward, err := NewPWMPin(forwardPin)
	if err != nil {
		return &Motor{}, err
	}
	backward, err := NewPWMPin(backwardPin)
	if err != nil {
		return &Motor{}, err
	}
	encoder, err := gpio.OpenPin(int(encoderPin), gpio.ModeInput)
	if err != nil {
		return &Motor{}, err
	}
	m := Motor{forward, backward, encoder, 0}
	encoder.BeginWatch(gpio.EdgeFalling, func() {
		m.absoluteSteps++
	})
	return &m, nil
}

func (m *Motor) getSteps() uint {
	return m.absoluteSteps
}

func (m *Motor) runContinuous(speed int) {
	if speed > 0 && speed <= 100 {
		m.backward.off()
		m.forward.set(uint8(speed))
	} else if speed < 0 && speed >= -100 {
		m.forward.off()
		m.backward.set(uint8(speed * -1))
	} else if speed == 0 {
		m.backward.off()
		m.forward.off()
	}
}
