package main

import "math"

type Motors struct {
	left        *Motor
	right       *Motor
	clearupChan chan uint8
}

// Left motor back
//pin1, err := NewPWMPin(15)
// Left motor forward
//pin2, err := NewPWMPin(13)
// Right motor forward
//pin3, err := NewPWMPin(14)
// Right motor back
//pin4, err := NewPWMPin(16)
//_, err = gpio.OpenPin(0, gpio.ModeInput)
//_, err = gpio.OpenPin(2, gpio.ModeInput)

func NewMotors() (*Motors, error) {
	mL, err := NewMotor(13, 15, 1)
	if err != nil {
		return &Motors{}, err
	}
	mR, err := NewMotor(14, 16, 0)
	if err != nil {
		return &Motors{}, err
	}
	m := Motors{mL, mR, make(chan uint8)}
	return &m, nil
}

func getSpeedMultiplierFromDifference(difference uint) float64 {
	normalizedDifference := math.Min(float64(difference)/100, 1)
	speedMultiplier := math.Min(-(math.Log10(normalizedDifference) / 1.1), 1)
	return speedMultiplier
}

func (m *Motors) clear() {
	select {
	case <-m.clearupChan:
		return
	default:
	}
	close(m.clearupChan)
}

func (m *Motors) runContinuous(speedL int, speedR int) {
	if speedL == speedR && speedL != 0 {
		m.runContinuousBinded(speedL)
	} else {
		m.runContinuousUnregulated(speedL, speedR)
	}
}

func (m *Motors) runContinuousUnregulated(speedL int, speedR int) {
	m.clear()
	m.left.runContinuous(speedL)
	m.right.runContinuous(speedR)
}

func (m *Motors) runContinuousBinded(speed int) {
	m.clear()
	leftStart := m.left.getSteps()
	rightStart := m.right.getSteps()
	stop := make(chan uint8)
	m.left.runContinuous(speed)
	m.right.runContinuous(speed)
	go func() {
		var leftCur uint
		var rightCur uint
		var leftCount uint
		var rightCount uint
		var difference uint
		for {
			leftCur = m.left.getSteps()
			rightCur = m.right.getSteps()
			leftCount = (leftCur - leftStart)
			rightCount = (rightCur - rightStart)
			difference = uint(math.Abs(float64(leftCount) - float64(rightCount)))
			if leftCount > rightCount {
				m.left.runContinuous(int(float64(speed) * getSpeedMultiplierFromDifference(difference)))
				m.right.runContinuous(speed)
			} else if leftCount < rightCount {
				m.left.runContinuous(speed)
				m.right.runContinuous(int(float64(speed) * getSpeedMultiplierFromDifference(difference)))
			} else {
				m.left.runContinuous(speed)
				m.right.runContinuous(speed)
			}
			select {
			default:
				// nothing
			case <-stop:
				return
			}
		}
	}()
	m.clearupChan = stop
}
