package main

import (
	"time"

	"github.com/Sijisu/gpio"
)

type PWMPin struct {
	pin           gpio.Pin
	dutyCycleChan chan uint8
}

func NewPWMPin(pinnumber uint8) (*PWMPin, error) {
	pin, err := gpio.OpenPin(int(pinnumber), gpio.ModeOutput)
	if err != nil {
		return &PWMPin{}, err
	}
	p := PWMPin{pin, make(chan uint8)}
	p.start()
	return &p, nil
}

func (p *PWMPin) run(dutycycle uint8) {
	for {
		//fmt.Println("dutyroutine running")
		select {
		case dutycycle = <-p.dutyCycleChan:
			// Special value to stop the routine
			if dutycycle == 255 {
				return
			}
		default:
			// nothing
		}
		if dutycycle != 0 {
			p.pin.Set()
		}
		time.Sleep(time.Microsecond * 50 * time.Duration(dutycycle))
		if dutycycle != 100 {
			p.pin.Clear()
		}
		time.Sleep(time.Microsecond * 50 * time.Duration(100-dutycycle))
	}
}

func (p *PWMPin) start() {
	go p.run(0)
}

func (p *PWMPin) stop() {
	p.dutyCycleChan <- 255
}

func (p *PWMPin) close() {
	p.pin.Close()
}

func (p *PWMPin) on() {
	p.dutyCycleChan <- 100
}

func (p *PWMPin) off() {
	p.dutyCycleChan <- 0
}

func (p *PWMPin) set(value uint8) {
	if value > 100 {
		value = 100
	}
	p.dutyCycleChan <- value
}
